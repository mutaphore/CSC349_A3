import java.util.*;
import java.io.*;

public class BipartiteTest {

   public static void main(String[] args) {
      GraphStart graph = new GraphStart();
      Scanner sc = new Scanner(System.in);
      String filename;
      Bipartite bp;

      try {
         System.out.print("Enter input file name: ");
         filename = sc.next();
         graph.readfile_graph(filename);
      }catch(FileNotFoundException e) {
         System.out.println("File not found: " + e.getMessage());
         return;
      }

      bp = new Bipartite(graph);
      bp.BFS();
      //Out put results
      System.out.print("Graph has (" + graph.nvertices + " vertices, " +
       graph.nedges + " edges) ");
      bp.PrintIsBi();
      bp.PrintCmps();
      //bp.PrintColors();
      //graph.print_graph();
   }

}
