import java.util.*;
import java.io.*;

public class Bipartite {
   private GraphStart graph;
   private int[] colors;   //0 for red, 1 for black
   private boolean[] visited;
   private boolean isBipartite;
   private ArrayList<LinkedList<Integer>> components;   // List of components

   //Constructor
   public Bipartite(GraphStart graph) {
      this.graph = graph;
      this.colors = new int[graph.nvertices + 1];
      this.visited = new boolean[graph.nvertices + 1];
      this.isBipartite = true;
      this.components = new ArrayList<LinkedList<Integer>>();
   }

   //Breadth first traversal of graph to determine its connected
   //components and whether it is bicolorable.
   public void BFS() {
      //Index 0 element in array is not used
      for (int v = 1; v <= graph.nvertices; v++) {
         if (visited[v] == false) {
            visited[v] = true;
            colors[v] = 0;   //Set first vertex to red
            bfs(v);
         }
      }
   }

   //BFS traversal of graph starting with v
   private void bfs(int v) {
      LinkedList<Integer> queue, cmp;
      ArrayList<Integer> adjList;
      ListIterator<Integer> itr;
      int front, adj, curColor;

      curColor = colors[v];
      queue = new LinkedList<Integer>();   //Queue for BFS traversal
      cmp = new LinkedList<Integer>();      //Create a component
      queue.addLast(v);                  //Initialize queue with vertex

      while (queue.peek() != null) {
         front = queue.removeFirst();
         cmp.add(front);   //Add vertex to the list of connected components
         adjList = graph.edges[front];
         curColor = (colors[front] + 1) % 2;   //Change color for adjacent v
         
         itr = adjList.listIterator();
         while (itr.hasNext()) {
            adj = itr.next();

            if (visited[adj] == false) {
               visited[adj] = true;
               colors[adj] = curColor;
               queue.addLast(adj);
               ChkColor(adj);
            }
         }
      }
      //add connected components to components list
      components.add(cmp);
   }

   //Checks the colors of adjacent vertices from v that have been visited.
   //If any adjacent vertices has the same color as v, then the graph
   //is not bicolorable.
   private void ChkColor(int v) {
      ArrayList<Integer> adjList;
      ListIterator<Integer> itr;
      int adj;

      adjList = graph.edges[v];
      itr = adjList.listIterator();

      while (itr.hasNext() && isBipartite == true) {
         adj = itr.next();
         if (visited[adj] == true && colors[v] == colors[adj])
            isBipartite = false;
      }
   }

   //Prints all connected components in the graph
   public void PrintCmps() {
      ListIterator<LinkedList<Integer>> itr1;
      ListIterator<Integer> itr2;

      itr1 = components.listIterator();
      System.out.println("It has " + components.size()
       + " connected components: ");
      while (itr1.hasNext()) {
         System.out.print("{");
         itr2 = itr1.next().listIterator();
         while (itr2.hasNext()) {
            System.out.print(itr2.next());
            if (itr2.hasNext())
               System.out.print(", ");
         }
         System.out.print("}");
         if (itr1.hasNext())
            System.out.print("; ");
      }
      System.out.println();
   }

   //Prints message telling whether graph is bipartite or not
   public void PrintIsBi() {
      if (isBipartite == true)
         System.out.print("is");
      else
         System.out.print("is not");
      System.out.print(" BiColorable.");
      System.out.println();
   }

   //Prints the colors of all vertices
   public void PrintColors() {
      System.out.println("Colors (0 red, 1 black) ");
      for (int i = 1; i <= graph.nvertices; i++) {
         System.out.print(i + ":  ");
         System.out.println(colors[i] + " ");
      }
   }

}
